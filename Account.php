<?php
namespace App;

abstract class Account
{
    public $name;
    public $city;
    abstract public function userInfo();
}

?>
